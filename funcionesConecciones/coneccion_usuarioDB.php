<?php

    require_once __DIR__.'/funciones.php';
   

    
    function FindUsuario($datos){
       
	try{
    
	
	    $pdo= conectarUsuarioDB();

    
    //armado de query
	    $sql="SELECT usuarios.id AS id,usuarios.nick AS nick,usuarios.passwd AS password ,roles.nombre AS rol FROM roles_usuarios LEFT JOIN usuarios ON roles_usuarios.usuario_id = usuarios.id JOIN roles ON roles.id = roles_usuarios.rol_id WHERE usuarios.nick LIKE :nick AND usuarios.passwd LIKE  :password ";
    
    //preparamos el estatements
	    $stmt=$pdo->prepare($sql);
    
    //tengo los datos y prepara los formatos asimilado 
	    $stmt->setFetchMode(PDO::FETCH_OBJ);
    //PDO::FETCH_OBJ   ( objeto )
    //PDO::FETCH_ASSOC ( arreglo )
        
    
    //sustituimos los valores de la query con el valor real
	    $stmt->bindParam(":nick", $datos->nick);
	    $stmt->bindParam(":password", $datos->password);

    
    
    //ejecutar la query en la base de datos
	  $stmt->execute();
    
	  $usuario= $stmt->fetch();
    //recuperar los datos y guarda en un array asociativo llave => valor
   //dato $clientes= $stmt->fetchAll();
    
	    return $usuario;
    
    
	} catch (PDOException $ex) {
	    throw new Exception('Error de coneccion'.$ex->getMessage());
	   	
	}
	    
	    
    
    }
    
    
    function FindPermisosDelUsuario($id) {
       
	try{
    
	
	    $pdo= conectarUsuarioDB();
       
    //armado de query
	    $sql="SELECT permisos.nombre AS permisos FROM roles_usuarios JOIN roles_permisos ON roles_permisos.rol_id = roles_usuarios.rol_id JOIN permisos ON  roles_permisos.permiso_id = permisos.id WHERE roles_usuarios.usuario_id = :id GROUP BY 1";
    
    //preparamos el estatements
	    $stmt=$pdo->prepare($sql);
    
    //tengo los datos y prepara los formatos asimilado 
	    $stmt->setFetchMode(PDO::FETCH_OBJ);
    //PDO::FETCH_OBJ   ( objeto )
    //PDO::FETCH_ASSOC ( arreglo )
        
    
    //sustituimos los valores de la query con el valor real
	    $stmt->bindParam(":id", $id);    
    
    //ejecutar la query en la base de datos
	   $stmt->execute();
    
	   $permisos=$stmt->fetchAll(PDO::FETCH_COLUMN, 0);
	   
	    //recuperar los datos y guarda en un array asociativo llave => valor
   //dato $clientes= $stmt->fetchAll();
	//array_change_key_case($permisos['permisos'], CASE_LOWER);
	   return $permisos;
	   
    
	} catch (PDOException $ex) {
	    throw new Exception('Error de coneccion'.$ex->getMessage());
	}
	    
	    
    
    }