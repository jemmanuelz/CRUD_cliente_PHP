<?php

require_once __DIR__.'/funciones.php';



//funcion que crea un cliente a partir de los datos recibidos
function createCliente($datos) {
    
       
    
try{
    
    $pdo=  conectarDB();
       
    
    //armado de query
    $sql="INSERT INTO clientes (`nombre`,`apellido`,`fechaNacimiento`,`activo`,`nacionalidad_id`) VALUES ( :nombre, :apellido, :fecha, :activo, :nacionalidad)";
    
    //preparamos el estatements
    $stmt=$pdo->prepare($sql);
    
    //tengo los datos y prepara los formatos asimilado 
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    //PDO::FETCH_OBJ   ( objeto )
    //PDO::FETCH_ASSOC ( arreglo )
    
    //sustituimos los valores de la query con el valor real
    $stmt->bindParam(":nombre", $datos['nombre']);
    $stmt->bindParam(":apellido", $datos['apellido']);
    $stmt->bindParam(":fecha", $datos['fechaNacimiento']);
    $stmt->bindParam(":activo", $datos['activo']);
    $stmt->bindParam(":nacionalidad", $datos['nacionalidad_id']);
    
    
    //ejecutar la query en la base de datos
    $stmt->execute();
    
    //recuperar los datos y guarda en un array asociativo llave => valor
   //dato $clientes= $stmt->fetchAll();
    
    
    
    
    
} catch (PDOException $ex) {
    echo 'Error de coneccion'.$ex->getMessage();
    return FALSE;
}
    return TRUE;
   
    
}
//funcion que da la baja logica del cliente recibe el id para su busqueda y modificacion
function deleteCliente($id) {
    
       
try{
    
    $pdo=  conectarDB();
       
    
    //armado de query
    $sql="UPDATE clientes SET activo = 0  WHERE id= :id";
    
    //preparamos el estatements
    $stmt=$pdo->prepare($sql);
    
    //tengo los datos y prepara los formatos asimilado 
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    //PDO::FETCH_OBJ   ( objeto )
    //PDO::FETCH_ASSOC ( arreglo )
    
    //sustituimos los valores de la query con el valor real
    $stmt->bindParam(":id", $id);
    
    //ejecutar la query en la base de datos
    $stmt->execute();
    
    //recuperar los datos y guarda en un array asociativo llave => valor
   //dato $clientes= $stmt->fetchAll();
    
} catch (PDOException $ex) {
    echo 'Error de coneccion'.$ex->getMessage();
    return FALSE;
}
    
    return TRUE;
    
}


//funcion que actualiza los datos de un cliente a partir de los datos recibidos ,busca y actualiza
function updateCliente($datos) {
    
          
    
try{
    
    $pdo=  conectarDB();
       
    
    //armado de query
    $sql="UPDATE clientes SET nombre=:nombre,apellido=:apellido,fechaNacimiento=:fechaNacimiento,activo=:activo,nacionalidad_id=:nacionalidad_id WHERE clientes.id=:id";
    
    //preparamos el estatements
    $stmt=$pdo->prepare($sql);
    
    //tengo los datos y prepara los formatos asimilado 
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    //PDO::FETCH_OBJ   ( objeto )
    //PDO::FETCH_ASSOC ( arreglo )
        
    
    //sustituimos los valores de la query con el valor real
    $stmt->bindParam(":id", $datos['id']);
    $stmt->bindParam(":nombre", $datos['nombre']);
    $stmt->bindParam(":apellido", $datos['apellido']);
    $stmt->bindParam(":fechaNacimiento", $datos['fechaNacimiento']);
    $stmt->bindParam(":activo", $datos['activo']);
    $stmt->bindParam(":nacionalidad_id", $datos['nacionalidad_id']);
    
    
    //ejecutar la query en la base de datos
    $stmt->execute();
    
    //recuperar los datos y guarda en un array asociativo llave => valor
   //dato $clientes= $stmt->fetchAll();
    
    
    
    
    
} catch (PDOException $ex) {
    echo 'Error de coneccion'.$ex->getMessage();
    return FALSE;
}
    return TRUE;
    
}

//busca un cliente a partir de el id recibido y retorna los datos
function findCliente($id) {
    
    try{
    
    $pdo=  conectarDB();
    
    //armado de query
    $sql="SELECT * FROM clientes WHERE id = :id LIMIT 1";
    
    //preparamos el estatements
    $stmt=$pdo->prepare($sql);
    
    //tengo los datos y prepara los formatos asimilado 
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    //PDO::FETCH_OBJ   ( objeto )
    //PDO::FETCH_ASSOC ( arreglo )
    
    //sustituimos los valores de la query con el valor real
    $stmt->bindParam(":id", $id);
    
    //ejecutar la query en la base de datos
    $stmt->execute();
    
    //recuperar los datos y guarda en un array asociativo llave => valor
    $cliente= $stmt->fetch();
    
    
    
    
    
} catch (PDOException $ex) {
    echo 'Error de coneccion'.$ex->getMessage();
}
    

return $cliente;
    
    
    
    
    
}



//funcion que da la baja logica del cliente recibe el id para su busqueda y modificacion
function activar_DesactivarCliente($id,$activo) {
    
       
try{
    
    $pdo=  conectarDB();
       
    
    //armado de query
    $sql="UPDATE clientes SET activo = :activo  WHERE id= :id";
    
    //preparamos el estatements
    $stmt=$pdo->prepare($sql);
    
    //tengo los datos y prepara los formatos asimilado 
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    //PDO::FETCH_OBJ   ( objeto )
    //PDO::FETCH_ASSOC ( arreglo )
    
    //sustituimos los valores de la query con el valor real
    $stmt->bindParam(":id", $id);
    $stmt->bindParam(":activo", $activo);
    //ejecutar la query en la base de datos
    $stmt->execute();
    
    //recuperar los datos y guarda en un array asociativo llave => valor
   //dato $clientes= $stmt->fetchAll();
    
    
    
    
    
} catch (PDOException $ex) {
    echo 'Error de coneccion'.$ex->getMessage();
    return FALSE;
}
    
    return TRUE;
    
}





//se conecta con la DB y  trae todos los clientes de la misma
function AllCliente() {
    
    
try{
    
    $pdo=  conectarDB();
    
    //armado de query
    $sql="SELECT clientes.*,nacionalidades.descripcion AS nacionalidad FROM clientes JOIN nacionalidades ON clientes.nacionalidad_id = nacionalidades.id ";
    
    //preparamos el estatements
    $stmt=$pdo->prepare($sql);
    
    //tengo los datos y prepara los formatos asimilado 
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    //PDO::FETCH_OBJ   ( objeto )
    //PDO::FETCH_ASSOC ( arreglo )
    
    //sustituimos los valores de la query con el valor real
   // $stmt->bindParam(":edad", $edad_cliente);
    
    //ejecutar la query en la base de datos
    $stmt->execute();
    
    //recuperar los datos y guarda en un array asociativo llave => valor
    $clientes= $stmt->fetchAll();
    
    
    
    
    
} catch (PDOException $ex) {
    echo 'Error de coneccion'.$ex->getMessage();
}
    

return $clientes;
    
    
    
}

