<?php

    include_once '../usuario/modelo/Usuario.php';
    
//funcion que conecta con la CD de clientes
function conectarDB() {
    
    $pdo=new PDO('mysql:host=localhost;dbname=clientes_db','clientes_app','jFXC6VJbjQtjspKJ');
    
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);//ver configuracion
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);//forzar a alertar del error
    $pdo->exec("SET NAMES UTF8");
    
    return $pdo;
    
}

//funcion que conecta con la CD de clientes
function conectarUsuarioDB() {
    
    $pdo=new PDO('mysql:host=localhost;dbname=usuarios_db','clientes_app','jFXC6VJbjQtjspKJ');
    
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);//ver configuracion
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);//forzar a alertar del error
    $pdo->exec("SET NAMES UTF8");
    
    return $pdo;
    
}



//funcion que valida que no tenga numero y cumpla con las letras permitidas 
function validarLetras($string) {
    $patron_texto = "/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ\s]+$/";
     if( !(preg_match($patron_texto, $string)) ){
	 return FALSE;
	  	 
     }else{
	 return TRUE;
     }
    
}
//valida que el valor no se nulo 
function validaRequerido($valor){
   if(trim($valor) == ''){
      return false;
   }else{
      return true;
   }
}
//valida que sea un numero 
function validarEntero($valor, $opciones=null){
   if(filter_var($valor, FILTER_VALIDATE_INT, $opciones) == FALSE){
      return false;
   }else{
      return true;
   }
}
 
//carga la ruta del para las paginas que necesite la ruta de los recursos
function CargarRuta() {
    return '../../../clientePHP/';
}
//funcion que redirecciona al index e indica la resultado de la operacion
function redireccionIndex($ok){
    header("Location: ./index.php?ok=".$ok);
    die();
} 

function redireccionLoggin($ruta,$error){
   
    if(strlen($ruta) != 0){
	$ruta.="usuario/";
	header("Location:".$ruta."controladores/loggin_controller.php");
    }else{
	header("Location:loggin_controller.php");
    }
    
    
    die();
} 



//funcion que calcula la edad a partir de una fecha 
function calcularEdad($fechanacimiento){
	list($ano,$mes,$dia) = explode("-",$fechanacimiento);
	$anio_diferencia  = date("Y") - $ano;
	$mes_diferencia = date("m") - $mes;
	$dia_diferencia   = date("d") - $dia;
	if ($dia_diferencia < 0 || $mes_diferencia < 0)
		$anio_diferencia--;
	return $anio_diferencia;
}


function serializar($datos) {
    return serialize($datos);
}

function deserializar($datos) {    
    return unserialize($datos);
}
function cerrarSession() {
    session_start();

    //cierro session
    session_unset();//setea todas las sssiones
    session_destroy();//liberar todas las sessiones
   // redireccionLoggin();
}

function controlarPermisos($permiso){
    
    session_start();
    $usuario= deserializar($_SESSION['usuario']);
   
    if(!(in_array($permiso, $usuario->permisos))){
	
	
	    redireccionIndex(-1);
	
	
	
	//redireccionLoggin($ruta);
    }
    
        
}