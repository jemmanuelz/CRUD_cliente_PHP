<?php

require_once __DIR__.'/funciones.php';

//se conecta con la DB y tra todos los registros de nacionalidad 
function allNacionalidades() {
    
        
try{
    
    $pdo= conectarDB();
    
    //armado de query
    $sql="SELECT * FROM nacionalidades";
    
    //preparamos el estatements
    $stmt=$pdo->prepare($sql);
    
    //tengo los datos y prepara los formatos asimilado 
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    //PDO::FETCH_OBJ   ( objeto )
    //PDO::FETCH_ASSOC ( arreglo )
    
    //sustituimos los valores de la query con el valor real
   // $stmt->bindParam(":edad", $edad_cliente);
    
    //ejecutar la query en la base de datos
    $stmt->execute();
    
    //recuperar los datos y guarda en un array asociativo llave => valor
    $nacionalidades= $stmt->fetchAll();
    
    
    
    
    
} catch (PDOException $ex) {
    echo 'Error de coneccion'.$ex->getMessage();
}
    

    return $nacionalidades;
    
    
    
}



//busca una determinada nacionalidad
function findNacionalidad($id) {
    
    try{
    
    $pdo=  conectarDB();
    
    //armado de query
    $sql="SELECT * FROM nacionalidades WHERE id = :id LIMIT 1";
    
    //preparamos el estatements
    $stmt=$pdo->prepare($sql);
    
    //tengo los datos y prepara los formatos asimilado 
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    //PDO::FETCH_OBJ   ( objeto )
    //PDO::FETCH_ASSOC ( arreglo )
    
    //sustituimos los valores de la query con el valor real
    $stmt->bindParam(":id", $id);
    
    //ejecutar la query en la base de datos
    $stmt->execute();
    
    //recuperar los datos y guarda en un array asociativo llave => valor
    $nacionalidad= $stmt->fetch();
    
    
    
    
    
} catch (PDOException $ex) {
    echo 'Error de coneccion'.$ex->getMessage();
}
    

return $nacionalidad->descripcion;
    
    
    
    
    
}

//Busca que exista la nacionalidad en la DB a partir del id 
//en caso que no la encuetre retorna falso

function ExistNacionalidad($id) {
    
    try{
    
    $pdo=  conectarDB();
    
    //armado de query
    $sql="SELECT * FROM nacionalidades WHERE id = :id LIMIT 1";
    
    //preparamos el estatements
    $stmt=$pdo->prepare($sql);
    
    //tengo los datos y prepara los formatos asimilado 
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    //PDO::FETCH_OBJ   ( objeto )
    //PDO::FETCH_ASSOC ( arreglo )
    
    //sustituimos los valores de la query con el valor real
    $stmt->bindParam(":id", $id);
    
    //ejecutar la query en la base de datos
    $stmt->execute();
   
    
    
    
    
} catch (PDOException $ex) {
    return FALSE;
}
    

    return TRUE;   
    
}

