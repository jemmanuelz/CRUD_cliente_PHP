<?php
//formulario de creacion de un nuevo cliente
    session_start();
//incluye la plantilla y el modal de para mostrar la baja en pantalla
    require_once  __DIR__.'/../../layout/template.php';
    require_once  __DIR__.'/../../layout/mensajes.php';
    require_once $ruta.'layout/barra_usuario.php';
    
    //si no viene por el create_controller muestra error
    if (!isset($create)):
	echo '<h2> ERROR </h2>';
	 die();
     endif; 
 
     
    
?>



<!-- formulario que incluye la pagina donde que se encuentra los datos a solicitar-->

<form action="../controladores/create_controller.php" method="POST" id="formCliente">
    
    <?php
	require '../partials/form_cliente.php';
    ?>
  
</form>
