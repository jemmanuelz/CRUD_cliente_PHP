<?php
    //funciones 
    session_start();
    require_once __DIR__.'/../../usuario/modelo/Usuario.php';
    require_once  __DIR__.'/../../funcionesConecciones/coneccion_clienteDB.php';
    require_once  __DIR__.'/../../funcionesConecciones/funciones.php';
    
    
    $permiso= 'delete';
    controlarPermisos($permiso);
    
    
    $id=$_POST['id']; //recibe el id del formulario de baja del cliente

    //si no hubo problema al darle de baja 
    //ok igual a true
    $ok=activar_DesactivarCliente($id, FALSE);
    
    
     redireccionIndex($ok);    //redirecciona con el mensaje correspondiente
	
    