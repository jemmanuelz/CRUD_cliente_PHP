<?php 
        
//incluye las funciones para procesar y conectar a DB
    require_once '../../usuario/modelo/Usuario.php';
    require_once  '../../funcionesConecciones/coneccion_clienteDB.php';
    require_once  '../../funcionesConecciones/funciones.php';
    
    session_start();
    
    $permiso= "select";
    controlarPermisos($permiso);
    
    
    $ruta=  CargarRuta();
    $usuario= deserializar($_SESSION['usuario']);

    $index=true;//funcion que sirve despues en index o listado para validar que la ruta es correcta
   
    $ok=$_GET['ok'];//obtiene el ok de todas las redirecciones que pasan por el listado 

    if(!(empty($ok))){ //sino hay es porque vino por primera vez 
	//en caso de una redireccion
	 if($ok != -1){//comprueba que se concreto la operacion con exito
	    $correcto= '<div class="alert alert-success">
		    <strong>Correcto!</strong> La operacion termino con exito.
		    </div>';
	    
	   
	}
	else if($ok == -1){
	    $correcto= '<div class="alert alert-danger">
		    <strong>Error!</strong> EL usuario con el nick  '.$usuario->nick.' no tiene el permiso para realizar la operacion.
		    </div>';
	}
	else{//en ccaso contrario indica que hubo un error
	    $correcto= '<div class="alert alert-danger">
		    <strong>Error!</strong> La operacion fallo.
		    </div>';
	}
	
    }
   
   
  
    $title="listado de Clientes";
    
    $clientes= allCliente();//obtiene todos los clientes de la DB  
   
    
  //sigue camino para el listado
    require_once __DIR__.'/../index_vista.php'; // => sino encuentra lanza un faltal error sin redireccion 
    die();
//
    