<?php

    session_start();
    
    require_once __DIR__.'/../../funcionesConecciones/coneccion_nacionalidadDB.php';
    require_once __DIR__.'/../../usuario/modelo/Usuario.php';
    require_once  __DIR__.'/../../funcionesConecciones/coneccion_clienteDB.php';
    require_once  __DIR__.'/../../funcionesConecciones/funciones.php';
    
    
    $permiso= 'create';	   
    controlarPermisos($permiso);
    $usuario=  deserializar($_SESSION["usuario"]);
    
    if ($_SERVER['REQUEST_METHOD'] == 'GET' && !(isset($_REQUEST['cliente'])) ) {
	
	
    
	
	 $create=true;
    
    //valiables locales
      
	$ruta=  CargarRuta();
	$nacionalidades=allNacionalidades();
    
    //redireccion
	$title= 'Nuevo Cliente';
    
    
	require_once __DIR__.'/../fomularios/create.php'; // => sino encuentra lanza un faltal error sin redireccion 
	die();
	
    }
    
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && !(isset($_REQUEST['id'])) ) {
	
	  
	$activo=$_POST['activo'];
	$errores=array();
	// $patron_texto = "/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ\s]+$/";
    
	if($activo == 'on'){
	    $activo=TRUE;
	
	}else{
	    $activo=FALSE;
	}
	    
	    
	    

	$cliente= [
		'nombre'=> $_POST['nombre'] ,
		'apellido'=> $_POST['apellido'],
		'activo'=> $activo,
		'nacionalidad_id' => $_POST['nacionalidad_id'],
		'fechaNacimiento' =>   date("Y-m-d",strtotime( $_POST['fechaNacimiento'] ))
	
	
		];
    
    
	if(!(validaRequerido($cliente['nombre']))){
	    $errores[]="el nombre es requerido";
	}
    
	if( !(validarLetras($cliente['nombre'])) ){
	     $errores[]="el nombre tiene que ser solo letras";
	 	 
	}
    
    
	if(!(validaRequerido($cliente['apellido']))){
	    $errores[]="el apellido es requerido";
	}
    
	if( !( validarLetras( $cliente['apellido'])) ){
	    $errores[]="el apellido tiene que./clienteController.php ser solo letras";
	 	 
	}
    
	if(!(validaRequerido($cliente['fechaNacimiento']))){
	    $errores[]="la fecha de nacimiento es requerida";
	}
   
    
	if(!(ExistNacionalidad($cliente['nacionalidad_id']))){
	    $errores[]="el nacionalidad invalida";
	}
    


	if(count($errores) == 0 ){
	//guardar el cliente
	
	
	    $ok=createCliente($cliente);
	
	    if($ok){
		redireccionIndex($ok);
	    }
	
	
	
	}
	else{
	    $correcto= '<div class="alert alert-danger">
		    <strong>ERROR!</strong> ERROR! La operacion fallo no se creo.
    		    </div>';
	}
	require_once __DIR__.'/create_controlller.php';
	die();
	
    }
  