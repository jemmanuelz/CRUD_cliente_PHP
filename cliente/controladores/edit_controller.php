<?php
    
    session_start();
    
    
    require_once __DIR__.'/../../funcionesConecciones/coneccion_nacionalidadDB.php';
    require_once __DIR__.'/../../usuario/modelo/Usuario.php';
    require_once  __DIR__.'/../../funcionesConecciones/coneccion_clienteDB.php';
    require_once  __DIR__.'/../../funcionesConecciones/funciones.php';

    $permiso='edit';
	   
    controlarPermisos($permiso);
    $usuario=  deserializar($_SESSION["usuario"]);
	
  
    if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_REQUEST['cliente']) ) {
	
	
	$edit=true; //funcion que sirve despues en el for edit para validar que la ruta es correcta
    
    //valiables locales
    
	$ruta=  CargarRuta(); //carfa la ruta
	$nacionalidades=allNacionalidades(); //carga todas las nacionalidades de la DB
    
    //redireccion
	$title= 'Editar Cliente'; //titulo a mostrar en el form
    
    
    
	$id = $_GET['cliente']; //obtiene el id del cliente a actualizar de la ruta que viene por GET
    
	if(isset($cliente)){ //en caso que hubo error en el update_controller y vuelve al edit_controller vuelve a tomar el id del cliente
	    $id=$cliente['id'];
	}
    
    
    
	$cliente=findCliente($id); //busca al cliente en la DB
   

	$cliente->fechaNacimiento= date("d-m-Y",strtotime( $cliente->fechaNacimiento )); //cambia el formato a la fecha para mostrar en el form
    
    //prepara la vista del checkbox activo ...en caso de activo se mostrar marcado,en caso contrario no muestra nada
	if ($cliente->activo == TRUE) {
	    $cliente->activo='checked';
	}else{
	    $cliente->activo='';
	}
  
	    
	    
	    
	    
    //redireccion
	require_once __DIR__.'/../fomularios/edit.php'; // => sino encuentra lanza un faltal error sin redireccion 

	 
	
	die();
	
    }
    
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_REQUEST['id']) ) {
	
	 
//obtiene el dato activo del cliente para porcesar si esta activo o no el cliente     
	$activo=$_POST['activo'];
	$errores=array(); //array donde se almacena los errores que surgen en la validacion
  
//verificacion del checkbox activo    
	if($activo == 'on'){
	    $activo=1;
	
	}else{
	    $activo=0;
	}
	
    
//armado del cliente para su actualizacion
	$cliente= [
		'id' => $_POST['id'],
		'nombre'=> $_POST['nombre'] ,
		'apellido'=> $_POST['apellido'],
		'activo'=> $activo,
		'nacionalidad_id' => $_POST['nacionalidad_id'],
		'fechaNacimiento' =>   date("Y-m-d",strtotime( $_POST['fechaNacimiento'] ))
	
	
		];
    
//validaciones 

    //requeridos
    /*
     * nombre
     * apellido
     * fecha de nacimiento
     */
       
    
    //formato de texto
    /*
     * nombre
     * apellido
     */
    
    
    //existencia en la DB
    /*
     * nacionalidad
     */
    
    
	if(!(validaRequerido($cliente['nombre']))){
	    $errores[]="el nombre es requerido";
	}
    
	if( !(validarLetras($cliente['nombre'])) ){
	    $errores[]="el nombre tiene que ser solo letras";
	 	 
	}
    
    
	if(!(validaRequerido($cliente['apellido']))){
	    $errores[]="el apellido es requerido";
	}
   
	if( !( validarLetras  ($cliente['apellido'])) ){
	    $errores[]="el apellido tiene que ser solo letras";
	 	 
	}
    
	if(!(validaRequerido($cliente['fechaNacimiento']))){
	    $errores[]="la fecha de nacimiento es requerida";
	}
   
    
	if(!(ExistNacionalidad($cliente['nacionalidad_id']))){
	    $errores[]="el nacionalidad invalida";
	}
    


	if(count($errores) == 0 ){ //si no encuentra errores 
     		
	    //actualiza el cliente
	    $ok=updateCliente($cliente);
	
	
	    if($ok){ //si no ocurrio error en la actualizacion redirecciona
		redireccionIndex($ok);
	    
	    }
	
	
	}
	else{ //si hubo error muestra mensaje en pantalla 
		$correcto= '<div class="alert alert-danger">
		    <strong>FALLO!</strong> ERROR! La operacion fallo no se creo.
		    </div>';
	}
	//vuelve al controlador de actualizar
	require_once  __DIR__.'/edit_controller.php';
	
	die();
	
    }
    
  