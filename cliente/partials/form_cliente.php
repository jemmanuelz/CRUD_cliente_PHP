             <!-- Cliente -->
	     <?php  if (isset($cliente)):echo"<in>";endif;?>
		<div class="panel panel-default">
		    <div class="panel-heading">Cliente <?php  if (isset($cliente)):echo $cliente->apellido.','.$cliente->nombre;endif;?></div>
		    <div class="panel-body">
      
      
	
			<input type="hidden" id="id" name="id" value="<?php  if (isset($cliente)):echo $cliente->id; endif;?>">
		

			<div class="form-group row" > 
			   
				<label for="apellido" class="col-sm-1 form-control-label"> Apellido(*):</label>
				<div class="col-sm-6">
				    <input type="text" name="apellido" value="<?php  if (isset($cliente)):echo $cliente->apellido; endif;?>" class="form-control" id="apellido"/>                       
				</div>
			 
			     
			     
			     
			     
                        
                        </div><br><br>
                        <div class="form-group row" >                       
                        
				<label for="nombre" class="col-sm-1 form-control-label"> Nombre(*):</label>
				<div class="col-sm-6">
				    <input type="text" name="nombre" value="<?php  if (isset($cliente)):echo $cliente->nombre; endif;?>" class="form-control" id ="nombre"/>                       
				</div>
			    
			    
			    
                       
                        </div><br>
                       <div class="form-group row">                       
                        
                        
			    
				 <label for="fechaNacimiento" class="col-sm-1 form-control-label">Fecha de Nacimiento(*)</label> 
                    <div class="col-sm-6">        
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa glyphicon glyphicon-calendar"></i>
                            </div>
                            <input type="text" name="fechaNacimiento" value="<?php  if (isset($cliente)):echo $cliente->fechaNacimiento; endif;?>" class="form-control" id="fechaNacimiento"/>                 
                        </div>
               
          
			    
			
			    
			    
			    
                        
                        </div>
                        </div><br>
                       
                        <div class="form-group row" >
			    
			    <label for="nacionalidad_id" class="col-sm-1 form-control-label">Nacionalidad:</label>
                        <div class="col-sm-6" >
                        
			    <select id="nacionalidad_id" name="nacionalidad_id" class="form-control">
				 <?php	foreach(  $nacionalidades as $n): ?>    
			
				<option value="<?php echo $n->id; ?>"
				     <?php if (isset($cliente)) {
				    if($cliente->nacionalidad_id == (int)$n->id){
					     echo 'selected';
		     
				    }  
				    
				} ?> > <?php echo $n->descripcion; ?> </option>
				 
				      
				     
			    <?php endforeach;?>
			    </select>
                            

                        </div>
                        </div><br>
			 <div class="form-group row">
			    <label for="activo" class="col-sm-1 form-control-label">Activo:</label>
                            <div class="col-sm-6" >
                        
				<input type="checkbox" name="activo" id="activo"<?php if (isset($cliente)): echo $cliente->activo; endif;?>>

                        </div>
                        </div>
                        <br>
                      
			<button type="submit" name='guardar' id='guardar' class="btn btn-sm btn-success">Enviar</button>
				
			 
			<a href="../../index.php" class="btn btn-sm btn-danger">Cancelar</a>
                    
      
  </div>
</div>
		
		
	
           


	    <script>
		
		$ = jQuery.noConflict();
		$(document).ready(function() {
		    
		    
		    $('input').iCheck({
			checkboxClass: 'icheckbox_polaris',
			radioClass: 'iradio_polaris',
			 increaseArea: '-10%' // optional
		    });
		    
		    $('#fechaNacimiento').datepicker({
			rtl: true,
			autoclose: true,
			todayHighlight: true,
			format: 'dd-mm-yyyy',
			language: 'es'
         
		    });
		    
		    
		    $("#guardar").bind("click", function (){
	 
		    $("#formCliente").validate({
                      rules:{
                        nombre: {
			    letterswithbasicpunc: true,
                            required: true,
                            minlength: 2,
                            maxlength: 100
                        },
                        apellido:{ 
			    letterswithbasicpunc: true,
                            required: true,
                            minlength: 2,
                            maxlength: 100
                            },
                        
                        fechaNacimiento:{
                            date: false,
			    dateNL : true,
			    required: true
                        }
                },
                messages:{
			'nombre':{
			    letterswithbasicpunc: "solo letras por favor",
                            required:"El campo no puede quedar vacio"
                        },
                        'apellido':{
			    letterswithbasicpunc: "solo letras por favor",
                            required:"El campo no puede quedar vacio"
                        },
			'fechaNacimiento':{
                            required:"El campo no puede quedar vacio"
                        }		                     
                        
                }


                });
            });
		    
		    
		    
		});
		
	    </script>