<?php 

    require_once $ruta.'layout/template.php'; 
    require_once $ruta.'layout/modal_detele.php';    
    require_once $ruta.'layout/barra_usuario.php';
    require_once $ruta.'layout/mensajes.php'; 
    
    if (!isset($index)):
	echo '<h2> ERROR </h2>';
	 die();
    endif; 

    require_once $ruta.'funcionesConecciones/funciones.php';

?>

<br>
<div>
    <a href="./create_controller.php" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span> Nuevo Cliente</a>
</div>
    

  <br>

  <div class="col-sm-3">

    <select id="nacionalidadSelect" class="form-control">
      <option value="All">
        Nacionalidad
      </option>
    </select>      
  </div><br><br>

<table id="tabla_cliente" class="table table-bordered dt-responsive  display" role="grid" cellspacing="0" width="100%" data-toggle="dataTable" data-form="deleteForm">
    
    <thead>
	<tr class="bg-info">
	    
	    <th>Cliente</th>
	    <th>Edad</th>
	    <th>Nacionalidad</th>
	    <th>Activo</th>
	    <th>Editar</th>
	    <th>Baja</th>
	    <th>Accion</th>
	    
	    
	</tr>
    </thead>
    
    <tfoot></tfoot>
    
    <tbody>
	<?php foreach ($clientes as $cliente ): ?>
	<tr>
	    <td>
		<?php echo $cliente->nombre.','.$cliente->apellido;?>
	    </td>
	    <td>
		<?php echo calcularEdad( $cliente->fechaNacimiento )?>
	    </td>
	    <td>
		<?php echo $cliente->nacionalidad ?>
	    </td>
	    <td>
		
		<input type="checkbox" name="activo" disabled id="activo"<?php  if ($cliente->activo == TRUE):echo 'checked'; endif;?>>
		
	    </td>
	    <td>
		<a href="./edit_controller.php?cliente=<?php echo $cliente->id;?> " class="btn btn-xs btn-warning "><span class="glyphicon glyphicon-pencil"></span></a>
	    </td>
	    <td>
		
		<form action="./delete_controller.php" method="POST" class="form-inline form-delete">
		    
		    <input type="hidden" id="id" name="id" value="<?php echo $cliente->id;?>">
		    <button type="submit" class="btn btn-xs btn-danger delete" name="delete_modal"<?php  if ($cliente->activo == FALSE):echo 'disabled'; endif;?>><span class="glyphicon glyphicon-trash"></span></button>
		    
		</form>
		
		
	    </td>
	     <td>
		
		 <form action="./activar_controller.php" method="POST" >
		    
		    <input type="hidden" id="id" name="id" value="<?php echo $cliente->id;?>">
		    <input type="hidden" id="activo" name="activo" value="<?php echo $cliente->activo;?>">
		    <?php if($cliente->activo == TRUE):?>
		    <button type="submit" class="btn btn-xs btn-info " ><span class="glyphicon glyphicon-remove-circle"></span>DESACTIVAR</button>
		    <?php else: ?>
		    <button type="submit" class="btn btn-xs btn-info " ><span class="glyphicon glyphicon-ok-circle"></span>ACTIVAR</button>
		    <?php endif; ?>
		</form>
		
			
		
	    </td>
	    
	    
	    
	</tr>
	 <?php endforeach;?>
    </tbody>
    
    
</table>

	

  <script>

         $('table[data-form="deleteForm"]').on('click', '.form-delete', function(e){
                e.preventDefault();
                var $form=$(this);
                $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete-btn', function(){
                        $form.submit();
                });
            });
	    
	    
	    
	
        $ = jQuery.noConflict();
        $(document).ready(function() {
	    
	 
	    setTimeout(function() {
		$("#mensaje").fadeOut(3000);
	    },1000);
	    
	  $('input').iCheck({
			checkboxClass: 'icheckbox_polaris',
			radioClass: 'iradio_polaris',
			 increaseArea: '-10%' // optional
		    });
        
        var cliente_table = $('#tabla_cliente').DataTable({
            "language": {
 
              "sProcessing": "Procesando...", 
              "sLengthMenu": "Mostrar _MENU_ registros",
              "sZeroRecords": "No se encontraron resultados",
              "sEmptyTable": "Ningún dato disponible en esta tabla",
              "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix": "",
              "sSearch": "Buscar:",
              "sUrl": "",
              "sInfoThousands": ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst": "Primero",
                  "sLast": "Último",
                  "sNext": "Siguiente",
                  "sPrevious": "Anterior"
 
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
 
                }
 
          },
                    
          initComplete: function() {
            
            var api = this.api();
            var select = $('#nacionalidadSelect');
              
              api.column(2).data().unique().sort().each( function ( d, j ) {
                  select.append( '<option value="'+d+'">'+d+'</option>' )
              } );   
          }
        });


        $('#nacionalidadSelect').change(function() {

          var val = $.fn.dataTable.util.escapeRegex(
                      $(this).val()
                    );
            cliente_table.column(2)
                 .search( val == 'All' ? '' : '^'+val+'$', true, false )
                 .draw();
            });
        });
    
    </script>

