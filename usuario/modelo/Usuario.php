<?php

    require_once __DIR__.'/../../funcionesConecciones/coneccion_usuarioDB.php';
   
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of modelo
 *
 * @author jemmanuelz
 */
class Usuario {
    
    var $id;
    var $nick;
    var $password;
    var $permisos=array();
    var $registrado;
    var $rol;
    
    
    
    
    function Usuario($datos) {
	
	$this->nick=$datos['nick'];
	$this->password=$datos['password'];
	
	
    }
    
    function settNick($nick) {
	 $this->nick=$nick;
    }
    function settPassword($password) {
	 $this->password=$password;
    }
    
    
    function getNick() {
	return $this->nick;
    }
    function getPassword() {
	return $this->password;
    }
    function getPermirsos() {
	return $this->password;
    }
    
    
    function Registrado() {
	
	try {
	    $usuario=FindUsuario($this);
	   
	    $this->id= $usuario->id;
	    $this->rol=$usuario->rol;
	    
	}catch (Exception $e) {
	    $this->registrado= FALSE;	  
	    throw new Exception('error de coneccion'.$e->getMessage());
	  
	}

	    
			
    }
    
    
    function CargarPermisos(){
	
	if( $this->registrado != TRUE ){
	     throw new Exception('el usuario no esta registrado');
	}
		
	try {
	    $this->permisos= FindPermisosDelUsuario($this->id);
	} catch (UsuarioException $e) {
	    throw new Exception('error de coneccion'.$e->getMessage());
	}

	
		
		
    }
    
    
    
    
    
}
