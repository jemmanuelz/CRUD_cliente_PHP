
<?php
//formulario de creacion de un nuevo cliente
session_start();
//incluye la plantilla y el modal de para mostrar la baja en pantalla
    require_once  __DIR__.'/../layout/template.php';
    require_once __DIR__.'/../layout/mensajes.php';
    
    //si no viene por el create_controller muestra error
    if (!isset($loggin)):
	echo '<h2> ERROR </h2>';
	 die();
     endif; 
 
     
    
?>
<link rel="stylesheet" href="../../recursos/css/loggin.css"/>
<!-- formulario que incluye la pagina donde que se encuentra los datos a solicitar-->

<!-- LOGIN FORM -->
<div class="text-center" style="padding:50px 0">
    <div class="logo">Login del Usuario</div>
	<!-- Main Form -->
	<div class="login-form-1">
		<form id="login-form" class="text-left" action="loggin_controller.php" method="POST" >
			<div class="login-form-main-message"></div>
			<div class="main-login-form">
				<div class="login-group">
					<div class="form-group">
						<label for="nick" class="sr-only">Nick</label>
						<input type="text" class="form-control" id="nick" name="nick" placeholder="nick">
					</div>
					<div class="form-group">
						<label for="password" class="sr-only">Contraseña</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="password">
					</div>
					<div class="form-group login-group-checkbox">
						<input type="checkbox" id="recordar" name="recordar">
						<label for="recordar">recordar</label>
					</div>
				</div>
				<button type="submit" class="login-button"><i class="glyphicon glyphicon-chevron-right"></i></button>
			</div>
			<div class="etc-login-form">
				<p>Olvidaste tu contraseña? <a href="#">click aqui</a></p>
				<p>Registrarse? <a href="#">crear su nueva cuenta</a></p>
			</div>
		</form>
	</div>
	<!-- end:Main Form -->

</div>

<script>
      $("#login-form").validate({
	  
  	rules: {
	    nick: "required",
  	    password: "required",
	},
	messages:{
	    nick:{
		required:"El nick del usuario es obligatorio"
	    },
	    password:{
		required:"El password del usuario es obligatorio"
	    }
	},
  	errorClass: "form-invalid"
  });
</script>