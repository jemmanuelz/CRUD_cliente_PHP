<?php

    session_start();
    
   include_once  __DIR__. '/../modelo/Usuario.php';
   require_once __DIR__.'/../../funcionesConecciones/funciones.php';
  

    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	
	   $loggin=true;
	   $ruta=  CargarRuta();
	   $title= 'Usuario';
	  
	   require_once __DIR__. '/../loggin_vista.php';
	   die();
	
    }


    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	echo $_REQUEST['loggin'];
	
	$datos['nick']=$_POST['nick'];
	$datos['password']=$_POST["password"];
	
	$usuario=new Usuario($datos);
		    
	try {
	    
	    $usuario->Registrado();
	    $usuario->CargarPermisos();
	    
	    $_SESSION['usuario']=serializar($usuario);	
	
	    header('Location:../../cliente/controladores/index.php');	
	
	    
	    
	    
	} catch (Exception $e) {
	    cerrarSession();
	  
	    $error= $e->getMessage();
	    echo $error;
	    redireccionLoggin('');
	  
	   
	   
	}
	finally {
	     die();
	}
	
	
	
	
    }

    
    