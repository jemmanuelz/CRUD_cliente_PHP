<!DOCTYPE html>
<html>
    <head>
	<meta charset="UTF-8">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<title><?php  if (isset($title)): echo $title;  else: echo 'PHP INDEX';  endif;?> </title>
	
	
	<link rel="stylesheet" href="<?php  if (isset($ruta)): echo  $ruta;endif;?>recursos/css/datatables.min.css"/>
	<link  rel="stylesheet" href="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/bootstrap/css/bootstrap.min.css"/> 
	<link  rel="stylesheet" href="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/bootstrap/fonts/glyphicons-halflings-regular.eot"/>
	<link  rel="stylesheet" href="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/bootstrap/fonts/glyphicons-halflings-regular.svg"/>
	<link  rel="stylesheet" href="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/bootstrap/fonts/glyphicons-halflings-regular.woff"/>
	<link  rel="stylesheet" href="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/bootstrap/fonts/glyphicons-halflings-regular.ttf"/>
	<link  rel="stylesheet" href="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/datepicker/datepicker3.css"/>
	<link  rel="stylesheet" href="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/iCheck/polaris/polaris.css"/>
	
	
	<script type="text/javascript" src="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/js/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/js/datatables.min.js"></script>
	<script type="text/javascript" src="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/bootstrap/js/bootstrap.min.js"></script>
	
	<script type="text/javascript" src="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/js/additional-methods.js"></script>
	<script type="text/javascript" src="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/js/jquery-validate.js"></script>

	
	<script type="text/javascript" src="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/datepicker/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?php  if (isset($ruta)): echo $ruta;endif;?>recursos/iCheck/icheck.min.js"></script>
	
	
	
	
    </head>
    
    <body>
	<div>
	    
	</div>
	
    </body>
</html>