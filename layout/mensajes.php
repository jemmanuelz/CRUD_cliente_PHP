<?php if($correcto):?>
    <?php echo '<div id="mensaje">'.$correcto.'</div>';?>
<?php endif; ?>
	     
	     
	     
<?php if ($errores): ?>
    <div class="alert alert-warning alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong>Aviso!</strong> por favor revise los siguientes idem:
    </div>
    
    <div class="alert alert-danger" role="alert">
	    
        <ul>
	    <?php foreach ($errores as $error): ?>
	        <li> <?php echo $error ?> </li>
	    <?php endforeach; ?>
	</ul>
    </div>

<?php endif;?>
